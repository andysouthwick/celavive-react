# README #

The Celavive-react project

### What is this repository for? ###

* Build for celavive react
* Version 1.0.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* On download either run yarn install or npm install
* I would go and download yarn because it is so much faster
* aftern the packages are installed run yarn start or npm start
* The browser and all the packages will hot reload on any changes

### Who do I talk to? ###

* Andrew Southwick <andrew.southwick@us.usana.com>
* Kim Guanzon Joachim Guanzon <Kim.Guanzon@us.usana.com>
* Yan Chen <Yan.Chen@us.usana.com>